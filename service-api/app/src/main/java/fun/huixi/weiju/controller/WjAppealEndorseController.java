package fun.huixi.weiju.controller;


import fun.huixi.weiju.business.AppealBusiness;
import fun.huixi.weiju.pojo.entity.WjAppealEndorse;
import fun.huixi.weiju.service.WjAppealEndorseService;
import fun.huixi.weiju.util.CommonUtil;
import fun.huixi.weiju.util.wrapper.ResultData;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import fun.huixi.weiju.base.BaseController;

import javax.annotation.Resource;

/**
 * 记录诉求点赞
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@RestController
@RequestMapping("/wjAppealEndorse")
public class WjAppealEndorseController extends BaseController {


    @Resource
    private AppealBusiness appealBusiness;

    @Resource
    private WjAppealEndorseService appealEndorseService;


    /**
     *  给诉求点赞
     * @Author 叶秋
     * @Date 2021/11/16 10:41
     * @param appealId 诉求id
     * @return fun.huixi.weiju.util.wrapper.ResultData
     **/
    @PostMapping("appealEndorse/{appealId}")
    public ResultData appealEndorse(@PathVariable Integer appealId){

        Integer userId = CommonUtil.getNowUserId();

        // 判断此诉求用户是否点赞了
        Boolean orEndorse = appealEndorseService.judgeUserOrEndorse(userId, appealId);

        if(orEndorse){
            return ResultData.error("你已经点过赞了");
        }

        // 点赞该动态
        appealBusiness.endorseAppeal(userId, appealId);

        return ResultData.ok();
    }



    /**
     *  取消诉求点赞
     * @Author 叶秋
     * @Date 2021/11/16 10:41
     * @param appealId 诉求id
     * @return fun.huixi.weiju.util.wrapper.ResultData
     **/
    @PostMapping("appealCancelEndorse/{appealId}")
    public ResultData appealCancelEndorse(@PathVariable Integer appealId){

        Integer userId = CommonUtil.getNowUserId();

        // 判断此诉求用户是否点赞了
        Boolean orEndorse = appealEndorseService.judgeUserOrEndorse(userId, appealId);

        if(!orEndorse){
            return ResultData.error("还未点赞");
        }

        // 取消点赞
        appealBusiness.cancelEndorseAppeal(userId, appealId);

        return ResultData.ok();
    }




}

