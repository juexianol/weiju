package fun.huixi.weiju.business;


import fun.huixi.weiju.pojo.entity.WjUserTag;
import fun.huixi.weiju.pojo.vo.user.UserRegisterVO;

import java.util.List;

/**
 * 用户的相关业务方法
 * @Author 叶秋
 * @Date 2021/11/8 23:49
 * @param
 * @return
 **/
public interface UserBusiness {



    /**
     *  用户注册方法
     * @Author 叶秋
     * @Date 2021/11/10 23:30
     * @param userRegisterVO
     * @return void
     **/
    void userRegister(UserRegisterVO userRegisterVO);
}
