package fun.huixi.weiju.service.impl;

import fun.huixi.weiju.pojo.entity.Area;
import fun.huixi.weiju.mapper.AreaMapper;
import fun.huixi.weiju.service.AreaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Service
public class AreaServiceImpl extends ServiceImpl<AreaMapper, Area> implements AreaService {

}
