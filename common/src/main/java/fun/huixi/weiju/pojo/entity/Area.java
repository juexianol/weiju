package fun.huixi.weiju.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import fun.huixi.weiju.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("area")
public class Area extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 地区码
     */
    @TableId("code")
    private Integer code;

    /**
     * 地区名称
     */
    @TableField("name")
    private String name;

    /**
     * 父级地区码
     */
    @TableField("parent_code")
    private Integer parentCode;

    /**
     * 区域类型(0无 1国家 2省 3市 4区 5街道)
     */
    @TableField("area_type")
    private Integer areaType;


}
