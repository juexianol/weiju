package fun.huixi.weiju.pojo.dto.appeal;


import fun.huixi.weiju.page.PageQuery;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 *  分页查询诉求评论 接收前端传递过来的参数
 * @Author 叶秋
 * @Date 2021/11/16 11:51
 * @param
 * @return
 **/
@Data
public class PageQueryAppealCommentDTO extends PageQuery {


    // 分页参数已经继承

    @NotNull(message = "诉求id")
    private Integer appealId;


}
