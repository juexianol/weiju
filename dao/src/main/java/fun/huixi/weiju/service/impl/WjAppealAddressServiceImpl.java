package fun.huixi.weiju.service.impl;

import fun.huixi.weiju.pojo.entity.WjAppealAddress;
import fun.huixi.weiju.mapper.WjAppealAddressMapper;
import fun.huixi.weiju.service.WjAppealAddressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 存储诉求的地址信息 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Service
public class WjAppealAddressServiceImpl extends ServiceImpl<WjAppealAddressMapper, WjAppealAddress> implements WjAppealAddressService {

}
