package fun.huixi.weiju.mapper;

import fun.huixi.weiju.pojo.entity.WjUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjUserMapper extends BaseMapper<WjUser> {

}
