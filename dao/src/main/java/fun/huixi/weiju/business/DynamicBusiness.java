package fun.huixi.weiju.business;

import fun.huixi.weiju.page.PageData;
import fun.huixi.weiju.pojo.dto.dynamic.PageQueryDynamicCommentDTO;
import fun.huixi.weiju.pojo.dto.dynamic.QueryDynamicDTO;
import fun.huixi.weiju.pojo.dto.dynamic.SaveDynamicCommentDTO;
import fun.huixi.weiju.pojo.dto.dynamic.SaveDynamicDTO;
import fun.huixi.weiju.pojo.vo.dynamic.DynamicCommentItemVO;
import fun.huixi.weiju.pojo.vo.dynamic.DynamicListVO;

/**
 * 动态的相关业务方法
 *
 * @param
 * @Author Zol
 * @Date 2021/11/10
 * @return
 **/
public interface DynamicBusiness {

    /**
     * 查询 动态列表
     *
     * @param userId 当前用户ID
     * @param param
     * @return
     */

    /**
     *  查询 动态列表
     * @Author Zol
     * @Date 2021/11/16 10:35
     * @param userId 查询人id
     * @param param 查询参数
     * @return fun.huixi.weiju.page.PageData<fun.huixi.weiju.pojo.vo.dynamic.DynamicListVO>
     **/
    PageData<DynamicListVO> listPageDynamic(Integer userId, QueryDynamicDTO param);


    /**
     *  点赞动态
     * @Author 叶秋
     * @Date 2021/11/24 11:21
     * @param userId 用户id
     * @param dynamicId 动态id
     * @return void
     **/
    void endorseDynamic(Integer userId, Integer dynamicId);


    /**
     *  取消点赞的动态
     * @Author 叶秋
     * @Date 2021/11/29 14:36
     * @param userId 用户id
     * @param dynamicId 动态id
     * @return void
     **/
    void cancelDynamicEndorse(Integer userId, Integer dynamicId);


    /**
     * 分页查询动态参数
     * @Author 叶秋 
     * @Date 2021/12/10 10:28
     * @param userId 查询人用户id
     * @param pageQueryDynamicCommentDTO 查询参数
     * @return fun.huixi.weiju.page.PageData<fun.huixi.weiju.pojo.vo.dynamic.DynamicCommentItemVO>
     **/
    PageData<DynamicCommentItemVO> pageQueryDynamicComment(Integer userId, PageQueryDynamicCommentDTO pageQueryDynamicCommentDTO);

    /**
     *  保存动态评论
     * @Author 叶秋
     * @Date 2021/11/29 14:46
     * @param saveDynamicCommentDTO  动态评论相关参数
     * @return void
     **/
    void saveDynamicComment(SaveDynamicCommentDTO saveDynamicCommentDTO);


    /**
     *  删除动态评论
     * @Author 叶秋
     * @Date 2021/11/29 15:00
     * @param dynamicCommentId 动态评论id
     * @return void
     **/
    void removeDynamicComment(Integer dynamicCommentId);


    /**
     *  保存动态
     * @Author 叶秋
     * @Date 2021/11/29 15:18
     * @param userId 用户id
     * @param saveDynamicDTO 保存动态参数
     * @return void
     **/
    void saveDynamic(Integer userId, SaveDynamicDTO saveDynamicDTO);

}
