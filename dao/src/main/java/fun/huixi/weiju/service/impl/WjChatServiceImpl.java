package fun.huixi.weiju.service.impl;

import fun.huixi.weiju.pojo.entity.WjChat;
import fun.huixi.weiju.mapper.WjChatMapper;
import fun.huixi.weiju.service.WjChatService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 聊天室（聊天列表） 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Service
public class WjChatServiceImpl extends ServiceImpl<WjChatMapper, WjChat> implements WjChatService {

}
