package fun.huixi.weiju.service.impl;

import fun.huixi.weiju.pojo.entity.WjChatRecord;
import fun.huixi.weiju.mapper.WjChatRecordMapper;
import fun.huixi.weiju.service.WjChatRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 聊天室-聊天记录 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Service
public class WjChatRecordServiceImpl extends ServiceImpl<WjChatRecordMapper, WjChatRecord> implements WjChatRecordService {

}
