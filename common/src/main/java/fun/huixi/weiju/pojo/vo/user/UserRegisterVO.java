package fun.huixi.weiju.pojo.vo.user;


import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 *  用户注册前端传递过来的参数
 * @Author 叶秋 
 * @Date 2021/11/10 23:23
 * @param 
 * @return 
 **/
@Data
public class UserRegisterVO {

    /**
     * 昵称
     */
    @NotBlank(message = "昵称不能为空")
    private String nickName;

    /**
     * 性别 改用String，活泼一点。自定义都可以
     */
    private String sex;

    /**
     * 电子邮箱
     */
    private String email;

    /**
     * 密码
     */
    @NotBlank(message = "密码不能为空")
    private String password;

}
