package fun.huixi.weiju.service;

import fun.huixi.weiju.pojo.entity.WjUserTag;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户所存储的标签 服务类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjUserTagService extends IService<WjUserTag> {

}
