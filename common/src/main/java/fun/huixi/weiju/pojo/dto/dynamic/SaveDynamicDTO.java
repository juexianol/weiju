package fun.huixi.weiju.pojo.dto.dynamic;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 *  保存动态
 * @Author 叶秋
 * @Date 2021/11/29 15:09
 * @param
 * @return
 **/
@Data
public class SaveDynamicDTO {


    /**
     * 动态的内容（没有标题之类的）
     */
    @NotBlank(message = "动态的内容不能为空")
    private String content;

    /**
     * 动态标签，对应的字典id
     */
    private Integer tagDictId;

    /**
     * 动态所发素材的url地址，多个用逗号隔开
     */
    private String url;

}
