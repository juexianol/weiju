package fun.huixi.weiju.util.wrapper;


import fun.huixi.weiju.exception.ErrorCodeEnum;
import lombok.Data;

import java.io.Serializable;


/**
 * The class Wrapper.
 *
 * @param <T> the type parameter @author paascloud.net@gmail.com
 */
@Data
public class ResultData<T> implements Serializable {

    /**
     * 序列化标识
     */
    private static final long serialVersionUID = 4893280118017319089L;

    /**
     * 编号.
     */
    private String code;

    /**
     * 信息.
     */
    private String message;

    /**
     * 结果数据
     */
    private T result;

    protected ResultData(String code, String message, T data) {
        this.code = code;
        this.message = message;
        this.result = data;
    }

    /**
     * 成功返回结果
     *
     */
    public static <T> ResultData<T> ok() {
        return new ResultData<T>(ErrorCodeEnum.OK.getCode(), ErrorCodeEnum.OK.getMsg(), null);
    }

    /**
     * 成功返回结果
     *
     * @param data 获取的数据
     */
    public static <T> ResultData<T> ok(T data) {
        return new ResultData<T>(ErrorCodeEnum.OK.getCode(), ErrorCodeEnum.OK.getMsg(), data);
    }

    /**
     * 成功返回结果
     *
     * @param data 获取的数据
     * @param  message 提示信息
     */
    public static <T> ResultData<T> ok(T data, String message) {
        return new ResultData<T>(ErrorCodeEnum.OK.getCode(), message, data);
    }

    /**
     * 失败返回结果
     */
    public static <T> ResultData<T> error() {
        return new ResultData<T>(ErrorCodeEnum.ERROR.getCode(), ErrorCodeEnum.ERROR.getMsg(), null);
    }

    /**
     * 失败返回结果
     * @param message 提示信息
     */
    public static <T> ResultData<T> error(String message) {
        return new ResultData<T>(ErrorCodeEnum.ERROR.getCode(), message, null);
    }

    /**
     * 失败返回结果
     */
    public static <T> ResultData<T> error(ErrorCodeEnum errorCodeEnum) {
        return new ResultData<T>(errorCodeEnum.getCode(), errorCodeEnum.getMsg(), null);
    }

    /**
     * 失败返回结果
     */
    public static <T> ResultData<T> error(ErrorCodeEnum errorCodeEnum, String message) {
        return new ResultData<T>(errorCodeEnum.getCode(), message, null);
    }


    /**
     * 参数验证失败返回结果
     */
    public static <T> ResultData<T> validateFailed() {
        return error(ErrorCodeEnum.VALIDATE_FAILED);
    }


    /**
     * 参数验证失败返回结果
     * @param message 提示信息
     */
    public static <T> ResultData<T> validateFailed(String message) {
        return new ResultData<T>(ErrorCodeEnum.VALIDATE_FAILED.getCode(), message, null);
    }

    /**
     * 登录异常返回结果
     */
    public static <T> ResultData<T> unauthorized(T data) {
        return new ResultData<T>(ErrorCodeEnum.UNAUTHORIZED.getCode(), ErrorCodeEnum.UNAUTHORIZED.getMsg(), data);
    }

    /**
     * 未授权返回结果
     */
    public static <T> ResultData<T> forbidden(T data) {
        return new ResultData<T>(ErrorCodeEnum.FORBIDDEN.getCode(), ErrorCodeEnum.FORBIDDEN.getMsg(), data);
    }

}
