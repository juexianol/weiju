package fun.huixi.weiju.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import fun.huixi.weiju.base.BaseController;

/**
 * 诉求素材表-存储素材涉及的图片，或者大文件 前端控制器
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@RestController
@RequestMapping("/wjAppealMaterial")
public class WjAppealMaterialController extends BaseController {

}

