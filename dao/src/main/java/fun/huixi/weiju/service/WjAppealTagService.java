package fun.huixi.weiju.service;

import fun.huixi.weiju.pojo.entity.WjAppealTag;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.Map;

/**
 * 诉求-对应标签 服务类
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjAppealTagService extends IService<WjAppealTag> {


    /**
     *  根据诉求标签id查询对应的诉求标签
     * @Author 叶秋
     * @Date 2021/11/10 22:45
     * @param appealTagIds 诉求id集合
     * @return 标签id <-> 标签详情
     **/
    Map<Integer, WjAppealTag> queryAppealTagByTagIds(Collection appealTagIds);


}
